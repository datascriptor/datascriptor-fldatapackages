# datascriptor-fldatapackages

a set of frictionless data packages for:

* [ ] elife Key Resource Table
* [ ] factorial design treatment group level reports (mean,sem,group sizes) for 2, 3 and 4 categorical variables
* [ ] design matrix tests + comparison tables for univariate tests